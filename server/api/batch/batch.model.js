'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;
var Mixed = _mongoose2['default'].Schema.Types.mixed;

var BatchSchema = new _mongoose2['default'].Schema({
  batchStart: {
    type: Date,
    'default': Date.now
  },
  batchEnd: {
    type: Date
  },
  quantity: Number,
  currentState: {
    type: ObjectId,
    ref: 'BinState'
  },
  states: [{
    type: ObjectId,
    ref: 'BinState'
  }],
  collections: [{
    type: ObjectId,
    ref: 'Collection'
  }],
  batchNun: Number
});

exports['default'] = _mongoose2['default'].model('Batch', BatchSchema);
module.exports = exports['default'];
//# sourceMappingURL=batch.model.js.map
