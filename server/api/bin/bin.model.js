'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;

var BinSchema = new _mongoose2['default'].Schema({
  binNumbers: [Number],
  capacity: Number,
  size: {
    width: Number,
    height: Number,
    depth: Number
  },
  batch: {
    type: ObjectId,
    ref: 'Batch'
  }
});

exports['default'] = _mongoose2['default'].model('Bin', BinSchema);
module.exports = exports['default'];
//# sourceMappingURL=bin.model.js.map
