'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;
var Mixed = _mongoose2['default'].Schema.Types.Mixed;

var BinStateSchema = new _mongoose2['default'].Schema({
  bin: {
    type: ObjectId,
    ref: 'Bin'
  },
  binChild: Number,
  stateType: {},
  temperature: {
    type: Mixed,
    'default': false
  },
  volume: {
    type: Mixed,
    'default': false
  },
  moisture: {
    type: Mixed,
    'default': false
  },
  smell: {
    type: Mixed,
    'default': false
  },
  notes: {
    type: Mixed,
    'default': false
  },
  additions: {
    brown: {
      type: Mixed,
      'default': false
    },
    green: {
      type: Mixed,
      'default': false
    },
    water: {
      type: Mixed,
      'default': false
    },
    other: {
      type: Mixed,
      'default': false
    }
  }
});

exports['default'] = _mongoose2['default'].model('BinState', BinStateSchema);
module.exports = exports['default'];
//# sourceMappingURL=binState.model.js.map
