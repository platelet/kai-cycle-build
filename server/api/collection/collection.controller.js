/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/collections              ->  index
 * POST    /api/collections              ->  create
 * GET     /api/collections/:id          ->  show
 * PUT     /api/collections/:id          ->  update
 * DELETE  /api/collections/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.sum = sum;
exports.count = count;
exports.show = show;
exports.forSub = forSub;
exports.create = create;
exports.bulkCreate = bulkCreate;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _collectionModel = require('./collection.model');

var _collectionModel2 = _interopRequireDefault(_collectionModel);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Collections

function index(req, res) {
  var q = {};
  return _collectionModel2['default'].find(q).sort({ timestamp: -1 }).populate('subscription').batchSize(10000).exec().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a sum of Collections

function sum(req, res) {
  var q = {};
  return _collectionModel2['default'].find(q).sort({ timestamp: -1 }).batchSize(10000).exec().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a count of Collections

function count(req, res) {
  return _collectionModel2['default'].count().exec().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Collection from the DB

function show(req, res) {
  return _collectionModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

function forSub(req, res) {
  return _collectionModel2['default'].find({ subscription: req.params.id }).exec().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Collection in the DB

function create(req, res) {
  req.body.rider = ObjectId(req.body.rider._id);
  req.body.customer = ObjectId(req.body.customer._id);
  return _collectionModel2['default'].create(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

function bulkCreate(req, res) {
  var toInsert = [];
  req.body.forEach(function (q, i) {
    if (q.subscription.length > 10) q.subscription = ObjectId(q.subscription);else delete q.subscription;
    toInsert.push(q);
  });

  if (toInsert.length > 0) {
    _collectionModel2['default'].create(toInsert).then(respondWithResult(res, 201))['catch'](handleError(res));
  } else {
    respondWithResult(res, 201)({});
  }
}

// Updates an existing Collection in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _collectionModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Collection from the DB

function destroy(req, res) {
  return _collectionModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=collection.controller.js.map
