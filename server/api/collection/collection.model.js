'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;

var CollectionSchema = new _mongoose2['default'].Schema({
  subscription: {
    type: ObjectId,
    ref: 'Subscription'
  },
  rider: {
    type: ObjectId,
    ref: 'User'
  },
  amount: {
    type: Number,
    'default': 0
  },
  timestamp: {
    type: Date,
    'default': Date.now
  }
});

exports['default'] = _mongoose2['default'].model('Collection', CollectionSchema);
module.exports = exports['default'];
//# sourceMappingURL=collection.model.js.map
