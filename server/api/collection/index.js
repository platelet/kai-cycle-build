'use strict';

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

var _express = require('express');

var _collectionController = require('./collection.controller');

var controller = _interopRequireWildcard(_collectionController);

var _authAuthService = require('../../auth/auth.service');

var auth = _interopRequireWildcard(_authAuthService);

var router = new _express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/totalKG', auth.isAuthenticated(), controller.sum);
router.get('/totalCount', auth.isAuthenticated(), controller.count);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/:id/sub', controller.forSub);
router.post('/', auth.isAuthenticated(), controller.create);
router.post('/bulk', auth.isAuthenticated(), controller.bulkCreate);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router['delete']('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
//# sourceMappingURL=index.js.map
