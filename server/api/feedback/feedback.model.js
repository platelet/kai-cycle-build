'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;

var FeedbackSchema = new _mongoose2['default'].Schema({
  feedback: String,
  rating: Number,
  createdAt: {
    type: Date,
    'default': Date.now
  }
});

exports['default'] = _mongoose2['default'].model('Feedback', FeedbackSchema);
module.exports = exports['default'];
//# sourceMappingURL=feedback.model.js.map
