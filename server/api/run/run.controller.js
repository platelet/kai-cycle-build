/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/run              ->  index
 * POST    /api/run              ->  create
 * GET     /api/run/:id          ->  show
 * PUT     /api/run/:id          ->  update
 * DELETE  /api/run/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _runModel = require('./run.model');

var _runModel2 = _interopRequireDefault(_runModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Runs

function index(req, res) {
  return _runModel2['default'].find().exec().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Run from the DB

function show(req, res) {
  return _runModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Run in the DB

function create(req, res) {
  return _runModel2['default'].create(req.body).then(function (run) {
    console.log(run);
    return run;
  }).then(respondWithResult(res, 201))['catch'](handleError(res));
}

// Updates an existing Run in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _runModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Run from the DB

function destroy(req, res) {
  return _runModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=run.controller.js.map
