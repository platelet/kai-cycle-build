'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ObjectId = _mongoose2['default'].Schema.ObjectId;

var RunSchema = new _mongoose2['default'].Schema({
  name: String,
  customers: [{
    type: ObjectId,
    ref: 'Subscription'
  }],
  index: Number,
  day: String,
  suburb: String,
  active: Boolean
});

exports['default'] = _mongoose2['default'].model('Run', RunSchema);
module.exports = exports['default'];
//# sourceMappingURL=run.model.js.map
