/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/subscriptions              ->  index
 * POST    /api/subscriptions              ->  create
 * GET     /api/subscriptions/:id          ->  show
 * PUT     /api/subscriptions/:id          ->  update
 * DELETE  /api/subscriptions/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.createAdmin = createAdmin;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _mail = require('../../mail');

var _subscriptionModel = require('./subscription.model');

var _subscriptionModel2 = _interopRequireDefault(_subscriptionModel);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Subscriptions

function index(req, res) {
  return _subscriptionModel2['default'].find().sort('run.runIndex run.runPositionIndex').exec().then(respondWithResult(res))['catch'](handleError(res));
}

// Gets a single Subscription from the DB

function show(req, res) {
  return _subscriptionModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
}

// Creates a new Subscription in the DB

function create(req, res) {
  return _subscriptionModel2['default'].create(req.body).then(function (sub) {
    (0, _mail.sendWelcomeEmail)(sub.email, sub.name);
    return sub;
  }).then(respondWithResult(res, 201))['catch'](handleError(res));
}

function createAdmin(req, res) {
  if (req.user && req.user.role === 'admin') {
    return _subscriptionModel2['default'].create(req.body).then(function (sub) {
      return sub;
    }).then(respondWithResult(res, 201))['catch'](handleError(res));
  }
}

// Updates an existing Subscription in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _subscriptionModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

// Deletes a Subscription from the DB

function destroy(req, res) {
  return _subscriptionModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
}
//# sourceMappingURL=subscription.controller.js.map
