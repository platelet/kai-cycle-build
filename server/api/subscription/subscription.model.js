'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var SubscriptionSchema = new _mongoose2['default'].Schema({
  name: String,
  email: String,
  customerType: String,
  businessName: String,
  phone: String,
  binCount: Number,
  location: {
    suburb: String,
    streetAddress: String,
    streetLatLng: {
      lat: Number,
      lng: Number
    },
    binLatLng: {
      lat: Number,
      lng: Number
    },
    binLocationDescription: String
  },
  run: {
    runIndex: {
      type: Number,
      'default': 0
    },
    runPositionIndex: {
      type: Number,
      'default': 0
    }
  },
  notes: [{
    noteType: {
      type: String,
      'enum': ['account', 'collections', 'collection point', 'issues']
    },
    note: { type: String }
  }],
  status: {
    onWaitList: { type: Boolean, 'default': true },
    subscribed: { type: Boolean, 'default': false },
    onHold: { type: Boolean, 'default': false },
    emailVerified: { type: Boolean, 'default': false },
    addressVerified: { type: Boolean, 'default': false },
    onWave: { type: Boolean, 'default': false }
  },
  active: { type: Boolean, 'default': true },
  createdAt: { type: Date, 'default': Date.now }
});

exports['default'] = _mongoose2['default'].model('Subscription', SubscriptionSchema);
module.exports = exports['default'];
//# sourceMappingURL=subscription.model.js.map
