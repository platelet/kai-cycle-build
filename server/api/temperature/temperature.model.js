'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var TemperatureSchema = new _mongoose2['default'].Schema({
  temperature: { type: Number, 'default': 0.00 },
  bin: { type: Number, 'default': 1 },
  device_id: { type: String, 'default': 'device error' },
  timestamp: { type: Date, 'default': Date.now }
});

exports['default'] = _mongoose2['default'].model('Temperature', TemperatureSchema);
module.exports = exports['default'];
//# sourceMappingURL=temperature.model.js.map
