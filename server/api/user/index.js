'use strict';

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

var _express = require('express');

var _userController = require('./user.controller');

var controller = _interopRequireWildcard(_userController);

var _authAuthService = require('../../auth/auth.service');

var auth = _interopRequireWildcard(_authAuthService);

var router = new _express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/:id([0-9a-z]{24})', auth.hasRole('admin'), controller.show);
router.put('/:id([0-9a-z]{24})', auth.isAuthenticated(), controller.update);
router.put('/:id([0-9a-z]{24})/password', auth.isAuthenticated(), controller.changePassword);
router.post('/', auth.hasRole('admin'), controller.create);
router.post('/:code([0-9a-z]{8})/verify', controller.verifyAccount);
router.post('/:id([0-9a-z]{24})/reset', auth.isAuthenticated(), controller.reset);
router.post('/:code([0-9a-z]{8})/reset', controller.codeChangePassword);
router['delete']('/:id([0-9a-z]{24})', auth.hasRole('admin'), controller.destroy);

module.exports = router;
//# sourceMappingURL=index.js.map
