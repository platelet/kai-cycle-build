'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.create = create;
exports.validateCode = validateCode;
exports.verifyAccount = verifyAccount;
exports.show = show;
exports.destroy = destroy;
exports.reset = reset;
exports.changePassword = changePassword;
exports.codeChangePassword = codeChangePassword;
exports.update = update;
exports.me = me;
exports.authCallback = authCallback;

var _userModel = require('./user.model');

var _userModel2 = _interopRequireDefault(_userModel);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _configEnvironment = require('../../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _mail = require('../../mail');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function (err) {
    res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

/**
 * Get list of users
 * restriction: 'admin'
 */

function index(req, res) {
  if (req.user.role === 'admin') {
    return _userModel2['default'].find({}, '-salt -password').exec().then(function (users) {
      res.status(200).json(users);
    })['catch'](handleError(res));
  } else if (req.user.role === 'rider') {
    return _userModel2['default'].find({ role: 'customer' }, '-salt -password').exec().then(function (users) {
      res.status(200).json(users);
    });
  } else {
    return _userModel2['default'].findById(req.user._id, '-salt -password').exec().then(function (users) {
      res.status(200).json(users);
    })['catch'](handleError(res));
  }
}

/**
 * Creates a new user
 */

function create(req, res, next) {
  var newUser = new _userModel2['default'](req.body);
  newUser.save().then(function (user) {
    // var token = jwt.sign({ _id: user._id }, config.secrets.session, {
    //   expiresIn: 60 * 60 * 5
    // });
    // res.json({ token });
    res.status(201).json(user);
  })['catch'](validationError(res));
}

function validateCode(req, res, next) {
  var code = req.params.code;

  if (code.length !== 8) return res.status(204).end();

  _userModel2['default'].findOne({ verificationCode: code }).exec().then(function (user) {
    if (!user) {
      return res.status(204).end();
    }
    res.json(user.profile);
  })['catch'](function (err) {
    return next(err);
  });
}

function verifyAccount(req, res, next) {
  var code = req.params.code;

  if (code.length !== 8) return res.status(403).end();

  _userModel2['default'].findOne({ verificationCode: code }).exec().then(function (user) {
    if (!user) return res.status(403).json({ message: 'access denied' }).end();

    return res.status(200).json({ message: 'ok' }).end();
  });
}

/**
 * Get a single user
 */

function show(req, res, next) {
  var userId = req.params.id;

  return _userModel2['default'].findById(userId).exec().then(function (user) {
    if (!user) {
      return res.status(404).end();
    }
    res.json(user.profile);
  })['catch'](function (err) {
    return next(err);
  });
}

/**
 * Deletes a user
 * restriction: 'admin'
 */

function destroy(req, res) {
  return _userModel2['default'].findByIdAndRemove(req.params.id).exec().then(function () {
    res.status(204).end();
  })['catch'](handleError(res));
}

function reset(req, res, next) {
  var userId = req.params.id;
  if (req.user.role !== 'admin') {
    if (userId !== req.user._id) {
      return res.status(403).end();
    }
  }

  _userModel2['default'].findById(userId).exec().then(function (user) {
    if (user) {
      var code = Math.random().toString(36).slice(-8);
      user.verificationCode = code;
      (0, _mail.sendStaffResetEmail)(user.email, user.name, code);
      return user.save().then(function () {
        res.status(200).end();
      })['catch'](handleError(res, 500));
    } else {
      return res.status(403).end();
    }
  });
}

/**
 * Change a users password
 */

function changePassword(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  _userModel2['default'].findById(userId).exec().then(function (user) {
    if (user.authenticate(oldPass)) {
      user.password = newPass;
      return user.save().then(function () {
        res.status(204).end();
      })['catch'](validationError(res));
    } else {
      return res.status(403).json({}).end();
    }
  });
}

function codeChangePassword(req, res, next) {
  var verificationCode = req.params.code;
  var password = req.body.pwd;
  var confirm = req.body.cnf;
  if (password !== confirm) {
    return res.status(403).json({ message: 'Password Mismatch' }).end();
  }

  _userModel2['default'].findOne({ verificationCode: verificationCode }).exec().then(function (user) {
    if (user) {
      user.password = password;
      user.verificationCode = '';
      user.save().then(function () {
        return res.status(204).json({ message: 'password reset' }).end();
      })['catch'](validationError(res));
    } else {
      return res.status(403).json({ message: 'Access denied' }).end();
    }
  });
}

// Updates an existing Collection in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _userModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}

/**
 * Get my info
 */

function me(req, res, next) {
  var userId = req.user._id;

  return _userModel2['default'].findOne({ _id: userId }, '-salt -password').exec().then(function (user) {
    // don't ever give out the password or salt
    if (!user) {
      return res.status(401).end();
    }
    res.json(user);
  })['catch'](function (err) {
    return next(err);
  });
}

/**
 * Authentication callback
 */

function authCallback(req, res, next) {
  res.redirect('/');
}
//# sourceMappingURL=user.controller.js.map
