'use strict';

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

var _express = require('express');

var _xmasController = require('./xmas.controller');

var controller = _interopRequireWildcard(_xmasController);

var _authAuthService = require('../../auth/auth.service');

var auth = _interopRequireWildcard(_authAuthService);

var router = new _express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.get('/:id([0-9a-z]{24})', auth.hasRole('admin'), controller.show);
router.put('/:id([0-9a-z]{24})', controller.update);
router.post('/all', auth.hasRole('admin'), controller.generate);
router.post('/all/email', auth.hasRole('admin'), controller.emailAll);
router.post('/:id([0-9a-z]{24})/email', auth.hasRole('admin'), controller.emailSingle);
router.post('/:code([0-9a-z]{8})/verify', controller.verifyAccount);
router['delete']('/all', auth.hasRole('admin'), controller.destroyAll);
router['delete']('/:id([0-9a-z]{24})', auth.hasRole('admin'), controller.destroy);

module.exports = router;
//# sourceMappingURL=index.js.map
