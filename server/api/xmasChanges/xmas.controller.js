'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.generate = generate;
exports.emailAll = emailAll;
exports.emailSingle = emailSingle;
exports.verifyAccount = verifyAccount;
exports.show = show;
exports.destroy = destroy;
exports.destroyAll = destroyAll;
exports.update = update;

var _xmasModel = require('./xmas.model');

var _xmasModel2 = _interopRequireDefault(_xmasModel);

var _subscriptionSubscriptionModel = require('../subscription/subscription.model');

var _subscriptionSubscriptionModel2 = _interopRequireDefault(_subscriptionSubscriptionModel);

var _configEnvironment = require('../../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var _mail = require('../../mail');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function (err) {
    res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

/**
 * Get list of xmas changes
 * restriction: 'admin'
 */

function index(req, res) {
  return _xmasModel2['default'].find({}).populate('subscription').exec().then(function (users) {
    res.status(200).json(users);
  })['catch'](handleError(res));
}

/**
 * Creates a new user
 */

function generate(req, res, next) {
  _subscriptionSubscriptionModel2['default'].find({ 'status.subscribed': true }).exec().then(function (subscriptions) {
    console.log('subs found:', subscriptions.length);
    subscriptions.forEach(function (subscription) {
      var newChange = {
        subscription: subscription._id,
        accessCode: Math.random().toString(36).slice(-8)
      };
      var newXmas = new _xmasModel2['default'](newChange);
      newXmas.save();
    });
    return true;
  }).then(respondWithResult(res, 201))['catch'](validationError(res));
}

function emailAll(req, res, next) {
  return res.status(210).end();
  // Xmas.find({}).populate('subscription').exec()
  //   .then(xmass => {
  //
  //   })
}

function emailSingle(req, res, next) {
  var xmasId = req.params.id;

  return _xmasModel2['default'].findById(xmasId).populate('subscription').exec().then(function (xmas) {
    if (!xmas) {
      return res.status(404).end();
    }
    //send email
    var name = xmas.subscription.customerType === 'residential' ? xmas.subscription.name : xmas.subscription.businessName;
    (0, _mail.sendHolidayEmail)(xmas.subscription.email, name, xmas.accessCode);
    xmas.status.emailed = true;
    return xmas.save();
  }).then(respondWithResult(res))['catch'](function (err) {
    return next(err);
  });
}

function verifyAccount(req, res, next) {
  var code = req.params.code;

  if (code.length !== 8) return res.status(403).end();

  _xmasModel2['default'].findOne({ accessCode: code }).populate('subscription').exec().then(function (xmas) {
    if (!xmas) return res.status(200).json(false).end();

    return res.status(200).json(xmas).end();
  });
}

/**
 * Get a single user
 */

function show(req, res, next) {
  var userId = req.params.id;

  return _xmasModel2['default'].findById(userId).exec().then(function (user) {
    if (!user) {
      return res.status(404).end();
    }
    res.json(user.profile);
  })['catch'](function (err) {
    return next(err);
  });
}

/**
 * Deletes a user
 * restriction: 'admin'
 */

function destroy(req, res) {
  return _xmasModel2['default'].findByIdAndRemove(req.params.id).exec().then(function () {
    res.status(204).end();
  })['catch'](handleError(res));
}

function destroyAll(req, res) {
  return _xmasModel2['default'].remove({}).exec().then(function () {
    res.status(204).end();
  })['catch'](handleError(res));
}

// Updates an existing Collection in the DB

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _xmasModel2['default'].findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
}
//# sourceMappingURL=xmas.controller.js.map
