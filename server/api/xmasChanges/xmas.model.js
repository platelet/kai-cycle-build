'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

_mongoose2['default'].Promise = require('bluebird');

var ObjectId = _mongoose2['default'].Schema.ObjectId;

var XmasChangesSchema = new _mongoose.Schema({
  subscription: { type: ObjectId, ref: 'Subscription' },
  accessCode: { type: String },
  status: {
    emailed: { type: Boolean, 'default': false },
    viewed: { type: Boolean, 'default': false },
    completed: { type: Boolean, 'default': false }
  },
  changes: {},
  notes: String,
  createdAt: {
    type: Date,
    'default': Date.now
  }
});

exports['default'] = _mongoose2['default'].model('XmasChange', XmasChangesSchema);
module.exports = exports['default'];
//# sourceMappingURL=xmas.model.js.map
