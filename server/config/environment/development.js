'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/kaicycle-dev'
  },

  // Seed database on startup
  seedDB: false

};
//# sourceMappingURL=development.js.map
