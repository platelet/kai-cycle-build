'use strict';

// Production specific configuration
// =================================
module.exports = {
        // Server IP
        ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,

        // Server port
        port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080,

        // MongoDB connection options
        mongo: {
                uri: 'mongodb://' + process.env.MONGODB_USER + ':' + process.env.MONGODB_PASSWORD + '@mongodb/' + process.env.MONGODB_DATABASE || 'mongodb://localhost/kaicycle'
        }
};
//# sourceMappingURL=production.js.map
