'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'customer', 'rider', 'admin'],
  suburbs: [{
    plain: 'Newtown',
    mapId: 'NEWTOWN'
  }, {
    plain: 'Mt Victoria',
    mapId: 'MT_VIC'
  }, {
    plain: 'Mt Cook',
    mapId: 'MT_COOK'
  }, {
    plain: 'Te Aro',
    mapId: 'TE_ARO'
  }, {
    plain: 'Berhampore',
    mapId: 'BERHAMPORE'
  }]
};
//# sourceMappingURL=shared.js.map
