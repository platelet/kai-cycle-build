/******************************************************************************
 *                                                                            *
 * Kaicycle App                                                              *
 * (C) Copyright WorkerBe 2015-2018 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

// Set default node environment to development
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (env === 'development' || env === 'test') {
  // Register the Babel require hook
  require('babel-core/register');
}

// Export the application
exports = module.exports = require('./app');
//# sourceMappingURL=index.js.map
