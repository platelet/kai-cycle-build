'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _xoauth2 = require('xoauth2');

var _xoauth22 = _interopRequireDefault(_xoauth2);

// var smtpConfig = {
//   service: 'Gmail',
//   auth: {
//     XOAuth2: {
//       user: 'kaicycle@workerbe.co',
//       clientId: '411154310775-1r4vl5lir282ljan8kkiosfkis8qg18a.apps.googleusercontent.com',
//       clientSecret: 'Adodt4AkFUivXJFjOL70Xajw',
//       refreshToken: '1/RumLOEpUDkq9GFxl9Y8xHXrMML_-GHPIpFYxGiaB_aU'
//     }
//   }
// };
//
// var transporter = nodemailer.createTransport(smtpConfig);

var transporter = _nodemailer2['default'].createTransport('smtps://kaicycle%40workerbe.co:5HospitalRoad@smtp.gmail.com');

var hbs = require('nodemailer-express-handlebars');
var handlebars = hbs({
  viewEngine: {
    layoutsDir: "server/mail/layouts/",
    partialsDir: "server/mail/views/",
    defaultLayout: "mainLayout"
  },
  viewPath: "server/mail/views"
});
transporter.use('compile', handlebars);

function sendWelcomeEmail(_transporter) {
  var _transporter = _transporter;
  return function (to, name) {
    var mailOptions = {
      from: '"Team KaiCycle" <kaicycle@workerbe.co>',
      to: to,
      subject: 'Welcome to Kaicycle!',
      template: 'welcome',
      context: {
        name: name
      }
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message sent: ' + info.response);
    });
  };
}

function sendHolidayEmail(_transporter) {
  var _transporter = _transporter;
  return function (to, name, accessCode) {
    var link = undefined;
    var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    if (env === 'development' || env === 'test') {
      link = 'localhost:9000/holidays/' + accessCode.trim();
    } else {
      link = 'http://kaicycle.workerbe.co/holidays/' + accessCode.trim();
    }
    var mailOptions = {
      from: '"Kaicycle Admin" <kaicycle@workerbe.co>',
      to: to,
      subject: 'Holiday Period Collections',
      template: 'xmas',
      context: {
        name: name,
        link: link
      }
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message Sent: ' + info.response);
    });
  };
}

function sendStaffResetEmail(_transporter) {
  var _transporter = _transporter;
  return function (to, name, code) {
    var link = undefined;
    var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    if (env === 'development' || env === 'test') {
      link = 'localhost:9000/account/reset/' + code.trim();
    } else {
      link = 'http://kaicycle.workerbe.co/account/reset/' + code.trim();
    }
    var mailOptions = {
      from: '"Kaicycle Admin" <kaicycle@workerbe.co>',
      to: to,
      subject: 'Password Reset Request',
      template: 'staffPasswordReset',
      context: {
        name: name,
        link: link
      }
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message Sent: ' + info.response);
    });
  };
}

function mailWrapper(_transporter) {
  var _transporter = _transporter;
  return function (to, subject, text, html) {
    var mailOptions = {
      from: '"Team KaiCycle" <kaicycle@workerbe.co>',
      to: to,
      subject: subject,
      text: text,
      html: html
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) console.log(error);
      console.log('Message sent: ' + info.response);
    });
  };
}

exports.sendWelcomeEmail = sendWelcomeEmail(transporter);
exports.sendEmail = mailWrapper(transporter);
exports.sendStaffResetEmail = sendStaffResetEmail(transporter);
exports.sendHolidayEmail = sendHolidayEmail(transporter);
//# sourceMappingURL=index.js.map
