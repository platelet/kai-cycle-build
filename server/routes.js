/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsErrors = require('./components/errors');

var _componentsErrors2 = _interopRequireDefault(_componentsErrors);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

exports['default'] = function (app, passport) {
  // Insert routes below
  app.use('/api/subscriptions', require('./api/subscription'));
  app.use('/api/bins', require('./api/bin'));
  app.use('/api/temperatures', require('./api/temperature'));
  app.use('/api/collections', require('./api/collection'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/batches', require('./api/batch'));
  app.use('/api/binStates', require('./api/binState'));
  app.use('/api/runs', require('./api/run'));
  app.use('/api/xmasChange', require('./api/xmasChanges'));
  app.use('/api/feedback', require('./api/feedback'));

  app.use('/auth', require('./auth'));

  app.route('/geojson').get(function (req, res) {
    res.sendFile(_path2['default'].resolve(app.get('root') + '/../server/poly.geojson'));
  });

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(_componentsErrors2['default'][404]);

  // All other routes should redirect to the index.html
  app.route('/*').get(function (req, res) {
    res.sendFile(_path2['default'].resolve(app.get('appPath') + '/index.html'));
  });
};

module.exports = exports['default'];
//# sourceMappingURL=routes.js.map
