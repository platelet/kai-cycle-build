/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2017 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var _csprng = require('csprng');

var _csprng2 = _interopRequireDefault(_csprng);

var hash = (0, _shaJs2['default'])('sha256');

exports.hasher = function (string) {
  return hash.update(string, 'utf-8').digest('hex');
};
exports.salter = function () {
  return (0, _csprng2['default'])(160, 36);
};

exports.studentCode = function () {
  return Math.random().toString(36).replace(/[^0-9a-z]/g, '').substr(1, 5);
};
//# sourceMappingURL=index.js.map
